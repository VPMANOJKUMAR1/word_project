# Word Master Game Project
## Project Description
+ Secret five letter word chosen
+ Players have five guesses to figure out the secret word.
+ After five guesses player unable find the word, Lose.
+ If player guesses a letter that is in the word but not in the right place, It is shown as yellow.
+ If the player guesses a letter that in the right place, it is shown as green
+ If the player guesses the right word, the player wins and the game is over. 

## Technologies
+ Used the HTML, CSS and JavaScript to build the Project.
+ Used two APIs to get the word of the day and validates five letter words.
+ [API to get the word of the day]( https://words.dev-apis.com/word-of-the-day)
+ [API to Validate the word](https://words.dev-apis.com/validate-word)
## How to Install and Run the Project
+ To run the project just need any internet server. I will provide the link of project you can run on any server like, chrome, firefox etc.
+ [link of the project](https://fastidious-lily-41facb.netlify.app).
## Credits
+ Followed the Frontend Masters , a course by Brain Holt to accomplish the Project.
+ [link to web page](https://frontendmasters.com/courses/web-development-v3/).
